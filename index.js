const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const tasks = require('./app/tasks');
const users = require('./app/users');

const app = express();
app.use(express.json());
app.use(cors());

app.use('/users', users);
app.use('/tasks', tasks);

const port = 8000;

const run = async () => {
    await mongoose.connect('mongodb://localhost/todoList', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });

    app.listen(port, () => {
      console.log('We are port ' + port);
    });

    exitHook(async callback => {
      await mongoose.disconnect();
      console.log('mongoose disconnected');
      callback();
    });
};

run().catch(console.error);
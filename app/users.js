const express = require('express');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const user = new User(req.body);
    await user.save();
    return res.send(user);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post('/session', async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(401).send({error: 'User not found'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({error: 'Password is wrong'});
  }

  user.generateToken();

  await user.save()

  return res.send(user);
});

module.exports = router;
const express = require('express');
const Task = require('../models/Task');
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {
  try {
    const newTask = {...req.body, user: req.user._id};
    const task = new Task(newTask);
    await task.save();
    return res.send(task);
  } catch(e) {
    res.status(401).send({error: e});
  }
});

router.get('/', auth, async (req, res) => {
  const tasks = await Task.find({user: req.user._id});

  if (!tasks) {
    return res.status(401).send({error: 'no tasks'});
  }

  res.send(tasks);
});

router.put('/:id', auth, async (req, res) => {
  const task = await Task.findOne({_id: req.params.id});

  if (!task) {
    return res.status(400).send({error: 'not found'});
  }


  if (JSON.stringify(req.user._id) !== JSON.stringify(task.user)) {
    return res.status(400).send({error: 'not your task'});
  }

  task.status = req.body.status;
  task.title = req.body.title;

  await task.save();
  res.send(task);
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const task = await Task.findOne({_id: req.params.id});

    if (!task) {
      return res.status(400).send({error: 'not found'});
    }


    if (JSON.stringify(req.user._id) !== JSON.stringify(task.user)) {
      return res.status(400).send({error: 'not your task'});
    }

    await task.delete();

    res.send({message: 'deleted'});
  } catch (e) {
    res.status(400).send({error: e});
  }
});

module.exports = router;